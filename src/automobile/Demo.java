/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package automobile;
import java.util.Date;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Demo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Employee employee = new Employee();
        employee.setName("E1");
        employee.setEmployeeId("Id1");
        employee.setDateOfJoining(new Date());

        System.out.println("Employee details");
        System.out.println("Employee Id: " + employee.getEmployeeId());
        System.out.println("Employee Name: " + employee.getName());
        System.out.println("Employee Date of Joining: " + employee.getDateOfJoining());

        Tool tool = new Tool();
        boolean promotionDueThisYear = tool.isPromotionDueThisYear(employee);
        Double taxForCurrentYear = tool.calcIncomeTaxForCurrentYear(employee);
        System.out.println("Is promotion due for employee " + employee.getName() + ": " + promotionDueThisYear);
        System.out.println(String.format("Tax for current year for employee %s: %.2f" , employee.getName() ,taxForCurrentYear));
    }
}
