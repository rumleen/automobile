/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package automobile;
import java.util.Random;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Tool {
     public boolean isPromotionDueThisYear(Employee employee) {
        return false;
    }

    public Double calcIncomeTaxForCurrentYear(Employee employee) {
        return new Random().nextDouble() * 1000;
    }
}
