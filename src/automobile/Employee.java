/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package automobile;
import java.util.Date;
/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Employee {
    private String employeeId;
    private String name;
    private Date dateOfJoining;

    public String getEmployeeId() {
        return employeeId;
    }

    public String getName() {
        return name;
    }

    public Date getDateOfJoining() {
        return dateOfJoining;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDateOfJoining(Date dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }
}
